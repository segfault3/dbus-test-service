#!/bin/bash

set -eu

print_usage() {
  cat << EOF
usage: $0 [OPTIONS]

Options:
--system
    Install a system service. The default is to install a session service.
-t, --transient
    Install a transient service.
-h, --help
    Print this usage message.
EOF
}

# Parse arguments
while [ "$#" -gt 0 ]; do
case "$1" in
    -h|--help)
    print_usage
    exit 0
    ;;
    -t|--transient)
    TRANSIENT=1
    shift
    ;;
    --system)
    SYSTEM=1
    shift
    ;;
    *)    # unknown option
    echo "unknown option: $1" >&2
    print_usage
    exit 1
    ;;
esac
done

BIN_DIR="bin"
SERVICE_EXECUTABLE="dbus-test-service"
DATA_DIR="data"
DBUS_FILE="org.example.test.service"
SYSTEMD_FILE="test.service"
# The config file is only used for the system service, because the
# session bus allows access to the service by default, while the
# system bus requires a policy which specifies what traffic is allowed.
# See the section about <policy> in
# https://dbus.freedesktop.org/doc/dbus-daemon.1.html.
DBUS_CONFIG_FILE="org.example.test.conf"

if [ -n "${TRANSIENT:-}" ] && [ -n "${SYSTEM:-}" ]; then
  echo "Only one of the options --transient and --system can be used" >&2
  exit 1
fi

if [ -n "${SYSTEM:-}" ]; then
  if [ "$EUID" -ne 0 ]; then
    echo "You must be root to install a system service"
    exit 1
  fi

  # Install the D-Bus service file to the system service directory
  # as specified in https://dbus.freedesktop.org/doc/dbus-daemon.1.html:
  #
  #   /usr/local/share/dbus-1/system-services
  #
  install -D -m 644  "${DATA_DIR}/${DBUS_FILE}" \
    "/usr/local/share/dbus-1/system-services/${DBUS_FILE}"

  # Install the D-Bus config file to the system bus config directory.
  # While https://dbus.freedesktop.org/doc/dbus-daemon.1.html says that
  # the directory should be /usr/local/etc/dbus-1/system.d, in the man
  # page installed on Debian it says /etc/dbus-1/system.d instead, and
  # only the latter seems to be used by dbus-daemon on Debian.
  install -D -m 644  "${DATA_DIR}/${DBUS_CONFIG_FILE}" \
    "/etc/dbus-1/system.d/${DBUS_CONFIG_FILE}"


  # Install the systemd unit DBUS_FILE according to
  # https://www.freedesktop.org/software/systemd/man/systemd.unit.html:
  #
  #   /etc/systemd/system   System units created by the administrator
  #
  install -D -m 644 "${DATA_DIR}/${SYSTEMD_FILE}" \
    "/etc/systemd/system/${SYSTEMD_FILE}"

  # Install the service executable
  install -D -m 755 "${BIN_DIR}/${SERVICE_EXECUTABLE}" \
    "/usr/local/bin/${SERVICE_EXECUTABLE}"
elif [ -n "${TRANSIENT:-}" ]; then
  # Install the D-Bus service file to the transient session service directory
  # as specified in https://dbus.freedesktop.org/doc/dbus-daemon.1.html:
  #
  #   $XDG_RUNTIME_DIR/dbus-1/services
  #
  install -D -m 644  "${DATA_DIR}/${DBUS_FILE}" \
    "$XDG_RUNTIME_DIR/dbus-1/services/${DBUS_FILE}"

  # Install the systemd unit file to the transient config directory as specified
  # in https://www.freedesktop.org/software/systemd/man/systemd.unit.html:
  #
  #   $XDG_RUNTIME_DIR/systemd/user
  #
  install -D -m 644 "${DATA_DIR}/${SYSTEMD_FILE}" \
    "$XDG_RUNTIME_DIR/systemd/user/${SYSTEMD_FILE}"

  # Install the service executable
  install -D -m 755 "${BIN_DIR}/${SERVICE_EXECUTABLE}" \
    "${HOME}/bin/${SERVICE_EXECUTABLE}"
else
  # Install the D-Bus service file to the session service directory
  # as specified in https://dbus.freedesktop.org/doc/dbus-daemon.1.html:
  #
  #   $XDG_DATA_HOME/dbus-1/services, where XDG_DATA_HOME defaults to ~/.local/share
  #
  install -D -m 644 "${DATA_DIR}/${DBUS_FILE}" \
    "${HOME}/.local/share/dbus-1/services/${DBUS_FILE}"

  # Install the systemd unit DBUS_FILE according to
  # https://www.freedesktop.org/software/systemd/man/systemd.unit.html:
  #
  #   $XDG_CONFIG_HOME/systemd/user or $HOME/.config/systemd/user
  #
  install -D -m 644 "${DATA_DIR}/${SYSTEMD_FILE}" \
    "${HOME}/.config/systemd/user/${SYSTEMD_FILE}"

  # Install the service executable
  install -D -m 755 "${BIN_DIR}/${SERVICE_EXECUTABLE}" \
    "${HOME}/bin/${SERVICE_EXECUTABLE}"
fi

if [ -n "${SYSTEM:-}" ]; then
  # Reload the systemd system unit file
  systemctl daemon-reload

  # Reload the D-Bus system service
  systemctl reload dbus
else
  # Reload the systemd user unit file
  systemctl --user daemon-reload

  # Reload the D-Bus session service
  systemctl --user reload dbus
fi
